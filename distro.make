api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7.23"

; Add ES Profile
projects[es_starterkit][type] = profile
projects[es_starterkit][download][type] = git
projects[es_starterkit][download][url] = git@bitbucket.org:asgorobets/es-starterkit.git
projects[es_starterkit][download][branch] = master
